function MapSettings(option) {
    option = option || {};
    this.gridSize = option.gridSize || 10;
    this.mapSize = option.mapSize || 50;
    this.canvasSize = option.canvasSize || 500;
}

function GameSettings(option) {
    option = option || {};
    this.speed = option.speed || 100;
    this.win = option.win || 10;
    this.maxFood = option.maxFood || 1;
}