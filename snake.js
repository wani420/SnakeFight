function Snake(option) {

    option = option || {};

    this.name = option.name || "";
    this.direction = option.direction || 'Right';
    this.body = option.body || [[0, 0], [1, 0], [2, 0]];
    this.mapSize = option.mapSize || 50;
    this.keyMap = option.keyMap || {Up: 38, Down: 40, Left: 37, Right: 39};
    this.color = option.color || 'rgb(170, 170, 170)';
    this.score = 0;
    this.isSnakeEatsFoodWhenGoToPoint = undefined;

    this.setDirection = function (direction) {
        if (this.direction === 'Right' && direction === 'Left') {
            return;
        }
        else if (this.direction === 'Left' && direction === 'Right') {
            return;
        }
        else if (this.direction === 'Up' && direction === 'Down') {
            return;
        }
        else if (this.direction === 'Down' && direction === 'Up') {
            return;
        }
        this.direction = direction;
    };

    this.getHead = function () {
        return this.body[this.body.length - 1];
    };

    this.move = function () {
        var head = this.getHead();
        if (this.direction === 'Right') {
            var newHead = [head[0] + 1, head[1]];
        }
        else if (this.direction === 'Left') {
            newHead = [head[0] - 1, head[1]];
        }
        else if (this.direction === 'Up') {
            newHead = [head[0], head[1] - 1];
        }
        else if (this.direction === 'Down') {
            newHead = [head[0], head[1] + 1];
        }

        var maxCoordinator = this.mapSize - 1;
        if (newHead[0] > maxCoordinator) {
            newHead[0] = 0;
        }
        if (newHead[1] > this.mapSize) {
            newHead[1] = 0;
        }
        if (newHead[0] < 0) {
            newHead[0] = this.mapSize;
        }
        if (newHead[1] < 0) {
            newHead[1] = this.mapSize;
        }

        if (this.isSnakeEatsFoodWhenGoToPoint && typeof(this.isSnakeEatsFoodWhenGoToPoint) === 'function') {
            var isSnakeEatsFoodWhenGoToPoint = this.isSnakeEatsFoodWhenGoToPoint(newHead);
            this.body.push(newHead);
            if (!isSnakeEatsFoodWhenGoToPoint) {
                this.body.splice(0, 1);
            }
            else {
                this.score++;
            }
        }
    };
}